#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This file is part of the lite version of the SalvusMesh package intended to
produce meshes for AxiSEM3D. If you are looking for the full version head
over to http://mondaic.com.

:copyright:
    Copyright (C) 2016-2019 Salvus Development Team <www.mondaic.com>,
                            ETH Zurich
:license:
    GNU General Public License, Version 3 [academic use only]
    (http://www.gnu.org/copyleft/gpl.html)
"""
import numpy as np
from scipy.integrate import ode


def _integrate_clairaut_equation_first_order(r, xi_0, nsteps, rtol, r_0_in_m,
                                             r_in_m, epsilon_surf,
                                             get_rho_fct, scale):
    """
    Internal function being used by models_1D.
    """
    def clairaut_equation_first_order(r, y):
        epsilon, eta, xi = y
        # xi = g * r ** 2 / (4 * pi * G)
        rho = get_rho_fct(r / scale)

        depsilon_dr = eta
        deta_dr = 6. / r ** 2 * epsilon - \
            2. * rho * r ** 2 / xi * (eta + epsilon / r)
        dxi_dr = rho * r ** 2

        return [depsilon_dr, deta_dr, dxi_dr]

    # the solution is linear in the ellipticity in the center, so
    # 'shooting' as suggested by Dahlen & Tromp 1998 is trivial. Just
    # integrate once with epsilon[0] = 1. and then scale the result so that
    # epsilon[-1] matches the terminal condition (i.e. the ellipticity at
    # the surface).
    nr = len(r)
    epsilon = np.zeros(nr)
    epsilon[0] = 1.
    xi = np.zeros(nr)
    xi[0] = xi_0

    integrator = ode(clairaut_equation_first_order)
    integrator.set_integrator('dopri5', nsteps=nsteps, rtol=rtol)
    integrator.set_initial_value([epsilon[0], 0., xi[0]], r_0_in_m)

    # Do the actual integration, storing the ellipticity and the
    # gravitational potential
    for i in np.arange(nr - 1):
        integrator.integrate(r_in_m[i+1])

        if not integrator.successful():
            raise RuntimeError(
                "Integration Error while intgrating Clairaut's equation")

        epsilon[i+1], _, xi[i+1] = integrator.y

    # scale to match surface ellipticity
    epsilon_0 = epsilon_surf / epsilon[-1]
    epsilon *= epsilon_0

    return epsilon, xi