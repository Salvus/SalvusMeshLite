{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "required": [
    "mesh_type"
  ],
  "allOf": [
    {
      "properties": {
        "mesh_type": {
          "title": "Mesh Type",
          "describe": "Choose the type of mesh.",
          "type": "string",
          "enum": [
            "AxiSEM",
            "AxiSEMCartesian"
          ]
        }
      }
    },
    {
      "type": "object",
      "anyOf": [
        {
          "allOf": [
            {
              "properties": {
                "mesh_type": {
                  "description": "Generate an AxiSEM mesh.",
                  "enum": [
                    "AxiSEM"
                  ]
                },
                "basic": {},
                "advanced": {},
                "attenuation": {},
                "spherical": {},
                "chunk2D": {},
                "refinement": {}
              },
              "required": [
                "basic",
                "advanced",
                "attenuation",
                "spherical",
                "chunk2D",
                "refinement"
              ],
              "additionalProperties": false
            },
            {
              "$ref": "#/definitions"
            }
          ]
        },
        {
          "allOf": [
            {
              "properties": {
                "mesh_type": {
                  "description": "Generate a cartesian AxiSEM mesh.",
                  "enum": [
                    "AxiSEMCartesian"
                  ]
                },
                "basic": {},
                "advanced": {},
                "attenuation": {},
                "cartesian2Daxisem": {},
                "refinement": {}
              },
              "required": [
                "basic",
                "advanced",
                "attenuation",
                "cartesian2Daxisem",
                "refinement"
              ],
              "additionalProperties": false
            },
            {
              "$ref": "#/definitions"
            }
          ]
        }
      ]
    }
  ],
  "definitions": {
    "properties": {
      "basic": {
        "title": "Basic Parameters",
        "description": "The most important parameters.",
        "type": "object",
        "properties": {
          "model": {
            "type": "string",
            "title": "Model",
            "description": "Model Name for predefined models or filename for external model file.",
            "default": "prem_ani"
          },
          "period": {
            "type": "number",
            "title": "Dominant Period",
            "description": "The desired dominant period of the mesh.",
            "default": 150.0,
            "minimum": 0.0,
            "exclusiveMinimum": true
          }
        },
        "required": [
          "model",
          "period"
        ],
        "additionalProperties": false
      },
      "advanced": {
        "type": "object",
        "title": "Advanced Parameters",
        "description": "Advanced parameters for more control.",
        "properties": {
          "elements_per_wavelength": {
            "type": "number",
            "title": "Elements per Wavelength",
            "description": "Number of elements per wavelength. Used to calculate element sizes to satisfy the desired dominant period.",
            "default": 2.0,
            "minimum": 0.0,
            "exclusiveMinimum": true
          },
          "courant_number": {
            "type": "number",
            "title": "Courant Number",
            "description": "Courant Number used to estimate dt.",
            "default": 0.6,
            "minimum": 0.0,
            "exclusiveMinimum": true
          },
          "model_parameters": {
            "type": "array",
            "title": "Model Parameters",
            "description": "Model parameters to put into the mesh, defaults to the parametrization used by the background model.",
            "default": [],
            "items": {
              "type": "string",
              "enum": [
                "VP",
                "VS",
                "VSV",
                "VSH",
                "VPV",
                "VPH",
                "RHO",
                "QMU",
                "QKAPPA"
              ]
            },
            "uniqueItems": true
          },
          "axial_shrinking": {
            "type": "number",
            "title": "Axial element shrinking",
            "description": "Account for lesser accuracy of GJI integration at the axis by shrinking the elements",
            "default": 1.0,
            "minimum": 0.0,
            "exclusiveMinimum": true
          },
          "velocity_model_representation": {
            "type": "string",
            "title": "Velocity Model Representation",
            "description": "How to represent the velocity model on the mesh. element_nodes: values are stored on each node of each element, allows discontinuities and (bi/tri)linear variations, but produces large files. elements: one value per element, allows discontinuities but the model will be piecewise constant. nodes: one value per node, enforces a continuous model, using mean at values on discontinuities.",
            "default": "element_nodes",
            "enum": [
              "element_nodes",
              "elements",
              "nodes"
            ]
          },
          "model_spline_order": {
            "type": "integer",
            "title": "Model Spline Order",
            "description": "Spline order used for model interpolation in case of layered models. Ignored for PREM like polynomial models.",
            "default": 3,
            "minimum": 1,
            "maximum": 5
          },
          "compression": {
            "type": "boolean",
            "title": "Compress Mesh",
            "description": "If true, the final mesh will be compressed with gzip compression level 2. Compression is for the most part only interesting for constant values and the compression level does matter much in that case.",
            "default": true
          }
        },
        "required": [
          "elements_per_wavelength",
          "courant_number",
          "velocity_model_representation",
          "model_spline_order",
          "compression"
        ],
        "additionalProperties": false
      },
      "attenuation": {
        "type": "object",
        "title": "Attenuation Arguments",
        "description": "Parameters for the memory variable approximation of attenuation.",
        "properties": {
          "number_of_linear_solids": {
            "type": "integer",
            "title": "Number of Linear Solids",
            "description": "The number of standard linear solids used.",
            "default": 5,
            "minimum": 1
          },
          "frequencies": {
            "type": "array",
            "title": "Frequencies",
            "description": "Minimum and maximum frequencies in the attenuation band. If either is not set, it will be determined automatically. Will be overwritten by 'auto_band'.",
            "default": [
              0.001,
              1.0
            ],
            "items": {
              "type": "number",
              "minimum": 0,
              "exclusiveMinimum": true
            },
            "additionalItems": false,
            "minItems": 2,
            "maxItems": 2
          },
          "power_law_reference_frequency": {
            "type": "number",
            "title": "Power Law Reference Frequency",
            "description": "Reference frequency for power law Q = Q_0 * (f / f_0)^alpha, where Q_0 is taken from background model [Hz].",
            "default": 1.0,
            "minimum": 0.0,
            "exclusiveMinimum": true
          },
          "power_law_alpha": {
            "type": "number",
            "title": "Power Law Alpha",
            "description": "Exponent alpha for power law Q = Q_0 * (f / f_0)^alpha.",
            "default": 0.0
          },
          "auto_band": {
            "type": "boolean",
            "title": "Auto frequency bands",
            "description": "Automatically determine attenuation band [f_min, f_max]. Will overwrite any given frequencies.",
            "default": false
          }
        },
        "required": [
          "frequencies",
          "number_of_linear_solids",
          "power_law_alpha",
          "power_law_reference_frequency"
        ],
        "additionalProperties": false
      },
      "cartesian_common": {
        "properties": {
          "x": {
            "type": "array",
            "title": "X Range",
            "description": "Horizontal (x direction) dimension of the domain in km.",
            "default": [
              0.0,
              null
            ],
            "items": {
              "anyOf": [
                {
                  "type": "number"
                },
                {
                  "type": "null"
                }
              ]
            },
            "additionalItems": false,
            "minItems": 2,
            "maxItems": 2
          },
          "y": {
            "type": "array",
            "title": "Y Range",
            "description": "Horizontal (y direction) dimension of the domain in km. The second item defaults to the second item of the x dimension if not specified.",
            "default": [
              0.0,
              null
            ],
            "items": {
              "anyOf": [
                {
                  "type": "number"
                },
                {
                  "type": "null"
                }
              ]
            },
            "additionalItems": false,
            "minItems": 2,
            "maxItems": 2
          },
          "min_z": {
            "type": "number",
            "title": "Minimum Z Value",
            "description": "Minimum z value, of the domain, where z = 0 corresponds to the lowest discontinuity in the 1D model.",
            "default": 0.0,
            "minimum": 0.0
          }
        }
      },
      "cartesian2Daxisem": {
        "type": "object",
        "title": "AxiSEM Cartesian Arguments",
        "description": "Parameters for AxiSEM cartesian meshes.",
        "properties": {
          "x": {
            "type": "number",
            "title": "X Range",
            "description": "Horizontal (x direction) dimension of the domain in km.",
            "default": 0.0,
            "minimum": 0.0
          },
          "min_z": {"$ref": "#/definitions/properties/cartesian_common/properties/min_z"}
        },
        "required": [
          "x",
          "min_z"
        ],
        "additionalProperties": false
      },
      "spherical_common": {
        "properties": {
          "min_radius": {
            "type": "number",
            "title": "Mimumum Radius",
            "description": "Minimum radius in km.",
            "default": 0.0,
            "minimum": 0.0
          },
          "ellipticity": {
            "type": "number",
            "title": "Ellipticity",
            "description": "Ellipticity of the planet (WGS84: 0.0033528106647474805, GRS80: 0.003352810681182319, MARS: 0.00589)",
            "default": 0.0
          },
          "gravity": {
            "type": "boolean",
            "title": "Add Gravitational Acceleration",
            "description": "Add gravitational acceleration to the mesh.",
            "default": false,
            "minimum": 0.0
          }
        },
        "required": [
          "min_radius",
          "ellipticity",
          "gravity"
        ],
        "additionalProperties": false
      },
      "spherical": {
        "type": "object",
        "title": "Spherical Arguments",
        "description": "Parameters for spherical meshes.",
        "properties": {
          "min_radius": {"$ref": "#/definitions/properties/spherical_common/properties/min_radius"},
          "ellipticity": {"$ref": "#/definitions/properties/spherical_common/properties/ellipticity"},
          "gravity": {"$ref": "#/definitions/properties/spherical_common/properties/gravity"}
        },
        "required": [
          "min_radius",
          "ellipticity",
          "gravity"
        ],
        "additionalProperties": false
      },
      "refinement_common": {
        "properties": {
          "refinement_style1": {
            "type": "string",
            "title": "Refinement Style",
            "description": "The mesh refinement style.",
            "default": "doubling",
            "enum": [
              "doubling",
              "tripling",
              "doubling_single_layer"
            ]
          },
          "refinement_style2": {
            "type": "string",
            "title": "Refinement Style",
            "description": "The mesh refinement style.",
            "default": "doubling",
            "enum": [
              "doubling",
              "tripling"
            ]
          },
          "hmax_refinement": {
            "type": "number",
            "title": "h_max Refinement",
            "description": "criterion (vertical oversamping factor BEFORE the refinement) for moving refinement layers downwards to avoid small timesteps caused by refinement layers. Smaller values = more aggressive, needs to be > 1.",
            "default": 1.5,
            "minimum": 1.0,
            "exclusiveMinimum": true
          },
          "refinement_bottom_up": {
            "type": "boolean",
            "title": "Refinement Bottom Up",
            "description": "Top down approach means minimizing number of elements at the surface at the cost of more elements at the bottom (default). If the option refinement_bottom_up is set, the bottom up approach is used instead, that is minimizing number of elements at the bottom at the cost of more elements at the surface. Which one is more efficient depends on the velocity model and refinement style.",
            "default": false
          }
        }
      },
      "refinement": {
        "type": "object",
        "title": "Refinement Arguments",
        "description": "Parameters for refinement of the meshes.",
        "properties": {
          "refinement_style": {"$ref": "#/definitions/properties/refinement_common/properties/refinement_style2"},
          "hmax_refinement": {"$ref": "#/definitions/properties/refinement_common/properties/hmax_refinement"},
          "refinement_bottom_up": {"$ref": "#/definitions/properties/refinement_common/properties/refinement_bottom_up"}
        },
        "required": [
          "refinement_style",
          "hmax_refinement",
          "refinement_bottom_up"
        ],
        "additionalProperties": false
      },
      "chunk_common": {
        "properties": {
          "max_colatitude2D": {
            "type": "number",
            "title": "Maximmum Colatitude",
            "description": "Maximum colatitude of the mesh from the center.",
            "default": 180.0,
            "minimum": 0.0,
            "maximum": 180.0,
            "exclusiveMinimum": true
          },
          "max_colatitude3D": {
            "type": "number",
            "title": "Maximmum Colatitude",
            "description": "Maximum colatitude of the mesh from the center.",
            "default": 45.0,
            "minimum": 0.0,
            "maximum": 75.0,
            "exclusiveMinimum": true
          }
        }
      },
      "chunk2D": {
        "type": "object",
        "title": "Chunk2D Arguments",
        "description": "Parameters for 2D circular chunks.",
        "properties": {
          "max_colatitude": {"$ref": "#/definitions/properties/chunk_common/properties/max_colatitude2D"}
        },
        "required": [
          "max_colatitude"
        ],
        "additionalProperties": false
      }
    }
  }
}
