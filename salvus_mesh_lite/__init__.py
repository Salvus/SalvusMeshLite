#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This file is part of the lite version of the SalvusMesh package intended to
produce meshes for AxiSEM3D. If you are looking for the full version head
over to http://mondaic.com.

:copyright:
    Copyright (C) 2016-2019 Salvus Development Team <www.mondaic.com>,
                            ETH Zurich
:license:
    GNU General Public License, Version 3 [academic use only]
    (http://www.gnu.org/copyleft/gpl.html)
"""
import inspect
import pathlib

from .version import get_git_version

__version__ = get_git_version()
# The inspect module does not properly work in Cython so we set the module path
# here.
__module_root__ = \
    pathlib.Path(inspect.getfile(inspect.currentframe())).parent.absolute()


class SalvusMeshWarning(UserWarning):
    """
    Base class for all SalvusMesh warnings.
    """
    pass


class SalvusMeshDeprecationWarning(SalvusMeshWarning):
    """
    Warns about deprecated features.
    """
    pass


from .skeleton import Skeleton  # NoQa
from .models_1D import model  # NoQa
from .structured_grid_2D import StructuredGrid2D  # NoQa
from .mesh.run_mesher import run_mesher  # NoQa