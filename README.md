------------
# WRONG REPOSITORY!!

**THIS PROJECT HAS BEEN MOVED HERE: https://gitlab.com/swp_ethz/public/SalvusMeshLite**
------------
.
------------
.
------------
.
------------
.
------------
.
------------


# SalvusMesh Lite

This package contains the GPLv3 licensed lite version of the SalvusMesh
package. It is capable of generating meshes with all the bells and whistles
for usage in AxiSEM3D. If you are looking for the full version of the mesher
head over to http://mondaic.com.

## Installation

Trivial with `pip`:

```bash
$ pip install https://gitlab.com/Salvus/SalvusMeshLite/-/archive/master/SalvusMeshLite-master.zip
```

## Usage

Two steps:

```bash
$ python -m salvus_mesh_lite.interface AxiSEM --save_yaml=input.yaml
```

This will create a self-explanatory `input.yaml` YAML file. Edit it to your
satisfaction and run

```bash
$ python -m salvus_mesh_lite.interface --input_file=input.yaml
```
